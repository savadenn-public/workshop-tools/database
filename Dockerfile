FROM postgres:14-alpine

COPY ./docker/pg-init-scripts/* /docker-entrypoint-initdb.d/

ENV POSTGRES_MULTIPLE_DATABASES=workshop,db_000000 \
    POSTGRES_PASSWORD=!ChangeMe! \
    POSTGRES_USER=workshop